package ru.mda.crib;


import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Класс содержит WebView, отображающую данные из файла
 */
public class Write extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write);

        Intent intent = getIntent();
        String position = intent.getStringExtra("pathFile");
        WebView webView = findViewById(R.id.webView);
        webView.loadUrl(position);
    }

}
