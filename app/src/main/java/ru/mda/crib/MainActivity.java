package ru.mda.crib;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Класс представляет собой активити содержащую ListView
 *
 * @author Макаров Д.А 17ИТ18
 */
public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listView);

        String[] theme = getResources().getStringArray(R.array.theme);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.expandable_list, theme);
        listView.setAdapter(adapter);

        final Intent intent = new Intent(this, Write.class);

        final String[] pathFiles = getResources().getStringArray(R.array.files);

        /**
         * Метод передаёт путь к файлу, который открывается в новой активити
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                intent.putExtra("pathFile", pathFiles[position]);
                startActivity(intent);
            }
        });
    }
}
